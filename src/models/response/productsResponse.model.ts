export interface ProductsResponse {
    customer?:           Customer;
    accountSavings?:     Account[];
    accountObligation?:  Account[];
    accountCreditQuota?: Account[];
    accountChecking?:    Account[];
}

export interface Account {
    sourceIdentification?:            string;
    sourceAccount?:                   string;
    sourceAccountAlias?:              string;
    sourceAccountType?:               string;
    sourceAccountTypeDescription?:    string;
    productCode?:                     string;
    payDate?:                         string;
    minimumPayment?:                  string;
    maximumPayment?:                  string;
    paymentReference?:                string;
    totalBalance?:                    string;
    availableBalance?:                string;
    state?:                           string;
    descriptionState?:                string;
    creationDate?:                    string;
    updateDate?:                      string;
    accountDefault?:                  string;
    sourceAccountSubType?:            string;
    sourceAccountSubTypeDescription?: string;
    businessRulesValues?:             BusinessRulesValues;
    featuresValues?:                  FeaturesValues;
    enterpriseRulesValues?:           EnterpriseRulesValues;
}

export interface BusinessRulesValues {
    allowsPay?:           string;
    allowsOtherValuePay?: string;
    allowsMinimumPay?:    string;
    allowsMaximumPay?:    string;
    allowsCredits?:       string;
    allowsDebits?:        string;
    initialValue?:        string;
}

export interface EnterpriseRulesValues {
    dailyAmount?:       string;
    state?:             string;
    profile?:           string;
    transactionsRules?: TransactionsRule[];
    actions?:           Actions;
}

export interface Actions {
    executeTransaction?: string;
    queryProduct?:       string;
    queryExtracts?:      string;
    queryMovements?:     string;
}

export interface TransactionsRule {
    code?:                   string;
    codeSwitch?:             string;
    fileCode?:               string;
    nameTransaction?:        string;
    descriptionTransaction?: string;
    state?:                  boolean;
}

export interface FeaturesValues {
    allowsQR?:                     string;
    allowsQRWithoutAmount?:        string;
    allowsCP?:                     string;
    allowsMovementsQuery?:         string;
    allowsObligationDetailsViews?: string;
    allowsViewTotalBalance?:       string;
    allowsViewAvailableBalance?:   string;
    allowsViewMaximumPayment?:     string;
    allowsViewMinimumPayment?:     string;
    allowsViewPayDate?:            string;
}

export interface Customer {
    sourceIdentification?:     string;
    sourceTypeIdentification?: string;
    sourceEntity?:             string;
    sourceEntityName?:         string;
    firstName?:                string;
    secondName?:               string;
    surname?:                  string;
    secondSurname?:            string;
}
