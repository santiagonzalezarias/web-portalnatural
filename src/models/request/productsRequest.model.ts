export interface ProductsRequest {
    date?:                     string;
    hour?:                     string;
    channel?:                  string;
    sourceIdentification?:     string;
    sourceTypeIdentification?: string;
    sourceNumberAccount?:      string;
    sourceTypeAccount?:        string;
    sourceEntity?:             string;
    productCode?:              string;
    ip?:                       string;
    user?:                     string;
    sequence?:                 string;
    operation?:                string;
}