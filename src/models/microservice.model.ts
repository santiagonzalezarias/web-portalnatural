interface Microservice {
    name: string;
    url: string;
    controllers: Controller[];
  }
  
  interface Controller {
    name: string;
    actions: Action[];
  }
  
  interface Action {
    endpoint: string;
    version: string;
    scope: string;
  }
  