interface ErrorModel {
    code: number;
    message: string;
  }