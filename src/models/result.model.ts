interface Result<T> {
    success?: T;
    error?: ErrorModel;
  }