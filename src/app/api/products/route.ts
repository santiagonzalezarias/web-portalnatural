import { ProductsRequest } from "@/models/request/productsRequest.model"
import { ProductsResponse } from "@/models/response/productsResponse.model"
import productService from "@/services/products.service"
import { Cryptography } from "@/utils/cryptography";
import { HttpStatusCode } from "axios";
import { NextRequest, NextResponse } from "next/server"

export async function POST(req: NextRequest) {
	const body : string = await req.json();
    const bodyRequest : ProductsRequest = JSON.parse(Cryptography.decrypt(body))
	const products = await productService.getProducts<ProductsRequest, ProductsResponse>(bodyRequest);

    if(products.success){
        return new NextResponse(JSON.stringify(Cryptography.encrypt(JSON.stringify(products.success))), { status: HttpStatusCode.Ok })
    }
    if(products.error){
        return new NextResponse(JSON.stringify(products.error), { status: products.error.code })
    }
}
