"use client";
import { ProductsRequest } from '@/models/request/productsRequest.model';
import { ProductsResponse } from '@/models/response/productsResponse.model';
import { apiRequest } from '@/utils/apiRequest';
import React, { useState } from 'react';

const MyComponent = () => {
  const [products, setProducts] = useState<ProductsResponse | undefined>()  

  const handleSubmit = async (e : any) => {
    e.preventDefault();
    const productsRequest: ProductsRequest = {
      date: "20240321",
      hour: "114303",
      channel: "WEB",
      sourceIdentification: "900800701",
      sourceTypeIdentification: "NI",
      sourceEntity: "00000076",
      ip: "191.95.39.238",
      user: "santi",
      sequence: "2024032193513250",
      operation: "QUERY",
    };

    try {
      const response = await apiRequest<ProductsRequest, ProductsResponse>('products', productsRequest, 'POST');
      setProducts(response)

    } catch (e){
      console.log(e)
    }
  }


  return (
    <div>
      <p>Products</p>
      <button onClick={(e) => handleSubmit(e)}>
        Click me
      </button>
      {products && <div>{products.customer?.firstName}</div>}
    </div>
  );
};

export default MyComponent; 