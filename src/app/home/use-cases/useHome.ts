import { ProductsRequest } from "@/models/request/productsRequest.model";
import { ProductsResponse } from "@/models/response/productsResponse.model";
import ProductService from "@/services/products.service";


const productsRequest: ProductsRequest = {
  date: "20240321",
  hour: "114303",
  channel: "WEB",
  sourceIdentification: "900800701",
  sourceTypeIdentification: "NI",
  sourceEntity: "00000076",
  ip: "191.95.39.238",
  user: "santi",
  sequence: "2024032193513250",
  operation: "QUERY",
};

export const fetchProducts = async (): Promise<ProductsResponse | undefined> => {
  const products = await ProductService.getProducts<ProductsRequest, ProductsResponse>(productsRequest);
  return products.success;
};