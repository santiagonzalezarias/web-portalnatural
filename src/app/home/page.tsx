import { ProductCard } from "@/components/ProductCard/ProductCard";
import { fetchProducts } from "./use-cases/useHome";
import MyComponent from "./components/MyComponent";


const Home = async () => {
  const products = await fetchProducts();
  return (
    <>
      <main>
        <p>Productos</p>
        {products?.accountSavings?.map((product, index) => (
          <ProductCard key={index} product={product} />
        ))}
        <div>
          <MyComponent></MyComponent>
        </div>
      </main>
    </>
  );
};

export default Home;