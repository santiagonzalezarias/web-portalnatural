"use client";
import { Account } from "@/data/response/productsResponse.model";

interface Props {
    product : Account
}
export const ProductCard : React.FC<Props> = ({ product }) => {
	return (
		<div className="col-6 col-xl-4 col-lg-4  col-md-6 col-sm-12 pb-2">
			<div className="card card-container shadow-default">
				<div className="div-radius-top bg-primary" />
				<div className="card-body">
					<ul className="list-group list-group-flush">
						<li className="list-group-item">
							<div className="float-start">
								<label className="heavy-16 title-card d-flex">{product.sourceAccountAlias ? product.sourceAccountAlias : product.sourceAccountTypeDescription}</label>
								<label className="regular-16 color-primary">Nro. {product.sourceAccount}</label>
							</div>

							<div className="float-end">
								<button className="btn p-0" onClick={() => console.log(product)}>
									<i className="icon quick-response bg-primary mx-2 w-30-px h-30-px"></i>
								</button>
							</div>
						</li>
						<li className="list-group-item">
							<div className="float-start">
								<label className="bold-14 color-primary d-flex">Tipo:</label>
								<label className="bold-14 color-primary">Código de producto:</label>
							</div>
							<div className="float-end">
								<label className="regular-14 color-primary text-end d-flex">
									{" "}
									{product.sourceAccountSubTypeDescription ? product.sourceAccountSubTypeDescription : product.sourceAccountTypeDescription}
								</label>
								<label className="regular-14 color-primary text-end"> {product.productCode}</label>
							</div>
						</li>
						<li className="list-group-item">
							
								<>
									<label className="heavy-14 color-complementary-2 d-flex">Saldo disponible: </label>
									<label className="black-20">{product.availableBalance}</label>
								</>
							
							<i
								className={`icon filled-show-eye float-end`}
								onClick={() => {
									console.log("click")
								}}
							></i>
								<div className="d-flex">
									<label className="black-14 color-complementary-2 float-start">Saldo total: </label>
									<label className="regular-14 color-complementary-2 float-end"> {product.totalBalance}</label>
								</div>
							
								<div className="d-flex">
									<label className="black-14 color-complementary-2 float-start">Pago mínimo: </label>
									<label className="regular-14 color-complementary-2 float-end"> {product.minimumPayment}</label>
								</div>
								<div className="d-flex">
									<label className="black-14 color-complementary-2 float-start">Pago total: </label>
									<label className="regular-14 color-complementary-2 float-end"> {product.maximumPayment}</label>
								</div>
								<>
									<div>
										<label className="regular-14 color-complementary-2 float-start">Fecha limite de pago:</label>

										<label className="regular-14 color-complementary-2 float-end">{product.payDate}</label>
									</div>
								</>
								<a className="card-link link-action">
									Ver movimientos
								</a>
								<a href="#" className="card-link link-action">
									Ver detalle del crédito
								</a>
						</li>
						<li className="list-group-item">
							<label className="black-14">Estado: </label>
						</li>
					</ul>
				</div>
			</div>
		</div>
	)
}
