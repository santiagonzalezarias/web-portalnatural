import { post } from "@/utils/httpRequest";

class ProductService {

    async getProducts<ProductsRequest, ProductsResponse>(data: ProductsRequest) : Promise<Result<ProductsResponse>> {
        const result = await post<ProductsResponse>('Products', 'Products', 'Products', JSON.stringify(data), true);
        return result;
    }
}

const productService = new ProductService();
export default productService;