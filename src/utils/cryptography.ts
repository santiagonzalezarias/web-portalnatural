import CryptoJS from "crypto-js"

export class Cryptography {
	private static SECRET_KEY = CryptoJS.enc.Utf8.parse(process.env.NEXT_PUBLIC_SECRET_KEY!) // Clave secreta de 16 caracteres (128 bits)
	private static IV = CryptoJS.enc.Utf8.parse(process.env.NEXT_PUBLIC_IV!) // Vector de inicialización constante

	// Función para encriptar
	static encrypt(text: string): string {
		const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), Cryptography.SECRET_KEY, {
			iv: Cryptography.IV,
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7,
		})
		return encrypted.toString()
	}

	// Función para desencriptar
	static decrypt(encryptedText: string): string {
		const decrypted = CryptoJS.AES.decrypt(encryptedText, Cryptography.SECRET_KEY, {
			iv: Cryptography.IV,
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7,
		})
		return CryptoJS.enc.Utf8.stringify(decrypted)
	}
}
