import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import {microservices} from './constants/microservices';


const AUTH_SERVER = process.env.NEXT_PUBLIC_AUTH_SERVER;
const USERNAME = process.env.NEXT_PUBLIC_USERNAME;
const PASSWORD = process.env.NEXT_PUBLIC_PASSWORD;
const GRANT_TYPE = process.env.NEXT_PUBLIC_GRANT_TYPE;


async function getToken(microservice: string, controller: string, action: string): Promise<Token | null> {
  const microserviceData = microservices.find((m) => m.name === microservice);
  const controllerData = microserviceData?.controllers.find((c) => c.name === controller);
  const actionData = controllerData?.actions.find((a) => a.endpoint === action);

  if (!actionData) {
    return null;
  }

  const headers = {
    'grant_type': GRANT_TYPE!,
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': `Basic ${Buffer.from(`${USERNAME!}:${PASSWORD!}`).toString('base64')}`,
  };

  const data = {
    'grant_type': GRANT_TYPE!,
    'scope': actionData.scope,
  };

  try {
    const response : AxiosResponse = await axios.post(AUTH_SERVER!, data, { headers });
    return response.data as Token;
  } catch (error) {
    console.error('Error al obtener el token:', error);
    return null;
  }
}

async function makeRequest<T>(
  method: 'get' | 'post' | 'put' | 'delete',
  microservice: string,
  controller: string,
  action: string,
  data?: any,
  withAuth = false,
  queryParams?: { [key: string]: string }
): Promise<Result<T>> {
  const microserviceData = microservices.find((m) => m.name === microservice);
  const controllerData = microserviceData?.controllers.find((c) => c.name === controller);
  const actionData = controllerData?.actions.find((a) => a.endpoint === action);

  if (!microserviceData || !controllerData || !actionData) {
    return { error: { code: 404, message: 'Microservicio, controlador o acción no encontrada' } };
  }

  const url = `${microserviceData.url}/${actionData.version}/${controllerData.name}/${actionData.endpoint}`;
  const config: AxiosRequestConfig = {
    method,
    url,
    params: queryParams,
    data,
  };

  if (withAuth) {
    const token = await getToken(microservice, controller, action);
    if (!token) {
      return { error: { code: 401, message: 'Error al obtener el token' } };
    }
    config.headers = {
      'Authorization': `Bearer ${token.accessToken}`,
      'Content-Type': 'application/json',
    };
  } else {
    config.headers = {
      'Content-Type': 'application/json',
    };
  }

  try {
    const response: AxiosResponse<T> = await axios(config);
    return { success: response.data };
  } catch (error: any) {
    const errorModel: ErrorModel = {
      code: error.response.status,
      message: error.response.data.message || error.message,
    };
    return { error: errorModel };
  }
}

export async function get<T>(
  microservice: string,
  controller: string,
  action: string,
  withAuth = false,
  queryParams?: { [key: string]: string }
): Promise<Result<T>> {
  return makeRequest<T>('get', microservice, controller, action, undefined, withAuth, queryParams);
}

export async function post<T>(
  microservice: string,
  controller: string,
  action: string,
  data?: any,
  withAuth = false
): Promise<Result<T>> {
  return makeRequest<T>('post', microservice, controller, action, data, withAuth);
}

export async function put<T>(
  microservice: string,
  controller: string,
  action: string,
  data?: any,
  withAuth = false
): Promise<Result<T>> {
  return makeRequest<T>('put', microservice, controller, action, data, withAuth);
}

export async function deleteRequest<T>(
  microservice: string,
  controller: string,
  action: string,
  withAuth = false,
  queryParams?: { [key: string]: string }
): Promise<Result<T>> {
  return makeRequest<T>('delete', microservice, controller, action, undefined, withAuth, queryParams);
}