import { HttpStatusCode } from "axios";
import { Cryptography } from "./cryptography";

type HttpMethod = 'POST' | 'GET' | 'DELETE' | 'PUT';

export async function apiRequest<TRequest, TResponse>(endpoint: string, data: TRequest, method: HttpMethod): Promise<TResponse> {
  try {
    const encryptedData : string = Cryptography.encrypt(JSON.stringify(data));

    const response = await fetch(`/api/${endpoint}`, {
      method: method,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(encryptedData),
    });

    if (response.status != HttpStatusCode.Ok && response.status != HttpStatusCode.Created) {
      const error : ErrorModel = JSON.parse(await response.json());
      if(error)
        throw new Error(error?.message);
      else
        throw new Error('Error en la solicitud')
    }

    const encryptedResponse : string = await response.json();
    const decryptedResponse : string = Cryptography.decrypt(encryptedResponse);

    return JSON.parse(decryptedResponse) as TResponse;
  } catch (error) {
    throw error;
  }
}
