export const microservices: Microservice[] = [
    {
      name: 'Products',
      url: 'https://visionamosonlinedes.redcoopcentral.com:8088/Microservice.Products.Openbanking',
      controllers: [
        {
          name: 'Products',
          actions: [{ endpoint: 'Products', version: 'v1', scope: 'Products::Products' }],
        },
      ],
    },
    {
      name: 'QR',
      url: 'https://visionamosonlinedes.redcoopcentral.com:8088/Microservice.QR.Openbanking',
      controllers: [
        {
          name: 'QR',
          actions: [
            { endpoint: 'GenerateEMVco', version: 'v1', scope: '' },
            { endpoint: 'ReadEMVco', version: 'v1', scope: '' },
            { endpoint: 'ProcessTransactionEMVco', version: 'v1', scope: '' },
          ],
        },
      ],
    },
  ];